﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TresureBox : MonoBehaviour
{
    bool isOpen = false;
    Animator animator;
    GameObject Text_OpenTreasureBox;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        Text_OpenTreasureBox = GameObject.Find("Text_OpenTreasureBox");
        Text_OpenTreasureBox.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            Text_OpenTreasureBox.SetActive(false);
        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (!isOpen)
        {
            if (collider.gameObject.tag == "Player")
            {
                Text_OpenTreasureBox.SetActive(true);
                if (Input.GetKey(KeyCode.Z))
                {
                    isOpen = true;
                    animator.SetBool("isOpen", true);
                    Text_OpenTreasureBox.SetActive(true);
                }
            }
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            Text_OpenTreasureBox.SetActive(false);
        }
    }
}

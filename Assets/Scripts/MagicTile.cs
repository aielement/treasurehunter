﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicTile : MonoBehaviour
{
    enum TileColor
    {
        TRANSPARENT,TRANSPARENT_RED,TRANSPARENT_BLUE,TRANSPARENT_GREEN
    }
    TileColor tileColor = TileColor.TRANSPARENT;
    Material transparent;
    Material transparent_red;
    Material transparent_blue;
    Material transparent_green;

    // Start is called before the first frame update
    void Start()
    {
        transparent = Resources.Load<Material>("TransparentGray");
        transparent_red = Resources.Load<Material>("TransparentRed");
        transparent_blue = Resources.Load<Material>("TransparentBlue");
        transparent_green = Resources.Load<Material>("TransparentGreen");
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Renderer>().material = transparent;
    }
}

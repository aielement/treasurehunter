﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item
{
    string itemID;
    int count;

    public int Count { get { return count; }  }
    public Item(string _itemID,int itemCount)
    {
        itemID = _itemID;
        count = itemCount;
    }

    public void GetItem(int getCount)
    {
        count += getCount;
    }
    public void LostItem(int lostCount)
    {
        count -= lostCount;
    }
}

public class ItemManager : MonoBehaviour
{
    Dictionary<string, Item> itemDict = new Dictionary<string, Item>();

    public void GetItem(string itemID,int itemCount)
    {
        if (itemDict.ContainsKey(itemID))
        {
            itemDict[itemID].GetItem(itemCount);
        }
        else
        {
            itemDict[itemID] = new Item(itemID, itemCount);
        }
    }

    public void LostItem(string itemID, int lostCount)
    {
        if (itemDict.ContainsKey(itemID))
        {
            itemDict[itemID].LostItem(lostCount);
        }
    }

    public List<Item> getHaveItemList()
    {
        List<Item> itemList = new List<Item>();
        foreach (Item item in itemDict.Values)
        {
            if (item.Count > 0)
            {
                itemList.Add(item);
            }
        }
        return itemList;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

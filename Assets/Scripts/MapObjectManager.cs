﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject
{
    public string objectID;
    public float rotAngleRad;
    Vector3 pos;
}

public class MapObjectDictionary
{
    Dictionary<string, GameObject> mapObjects = new Dictionary<string, GameObject>();
}

public class MapObjectManager : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        var mapBase = GameObject.Find("MapBase");
        GameObject floor = Resources.Load<GameObject>("floor");
        GameObject wall = Resources.Load<GameObject>("wall");
        for (int w = 0; w < 20; w++)
        {
            for (int h = 0; h < 15; h++)
            {
                var mapFloor = Instantiate(floor, new Vector3(w*6, 0, h*6) , floor.transform.rotation,mapBase.transform);
            }
        }
        


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.CharacterController;

public class PlayerController : MonoBehaviour
{
    Animator animator;
    Rigidbody rb;
    vThirdPersonController controller;
    bool isMove = false;
    bool validRun = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<vThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(rb.velocity.magnitude);
        animator.SetFloat("speed", new Vector3(rb.velocity.x, rb.velocity.y, 0).magnitude);
        animator.SetBool("isJumping", controller.isJumping);
        animator.SetBool("isGrounded", controller.isGrounded);
        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            isMove = true;
        }
        else
        {
            isMove = false;
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            validRun = true;
        }
        else
        {
            validRun = false;
        }

        animator.SetBool("isMove", isMove);
        animator.SetBool("validRun", validRun);
    }
}

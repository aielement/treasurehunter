﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeTransit
{
    public bool up;
    public bool down;
    public bool right;
    public bool left;
    public EdgeTransit()
    {
        up = true;
        down = true;
        right = true;
        left = true;
    }
}

public class Mapchip
{
    public EdgeTransit edgeTransit=new EdgeTransit();
    public bool transit;
    public int materialNum = 0;
    public Mapchip()
    {
        transit = true;
    }
}


